const express = require('express')
var path = require('path');
var mongo = require('mongodb').MongoClient;
const rp = require('request-promise')
const exphbs = require('express-handlebars')
var bodyParser = require('body-parser');
var database = require('./config/database');
const csv = require('csv-parser');
const fastcsv = require('fast-csv');
const fs = require('fs');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const app = express();

app.use('/public', express.static(__dirname + '/public'));
app.set('view engine', 'jade');

app.get('/index', (req, res) => {
	res.render('index')
})

app.get('/process_data', (req, res) => {
	var data = []
	const date  = Date.now();
	fs.createReadStream('sample.csv')
	  .pipe(csv())
	  .on('data', (row) => {
		try {
		 if(row.First){
			data.push({First:row.First,Last:row.Last,Month:row.Month, Day:row.Day})
		 }
		}
		catch(err) {
			res.render('error', err)
		}
	  })
	  .on('end', () => {
		  console.log(data);
		  const ws = fs.createWriteStream("download/result-"+date+".csv");  
		fastcsv  
		  .write(data, { headers: true })
		  .pipe(ws);
		console.log('CSV file successfully processed');
	});
	res.render('updated',{response: {message:'CSV data processed successfully', flag:1, timestamp:date}})
})

app.get('/get_data', (req, res) => {
	
	rp({
		uri: 'https://api.myjson.com/bins/bvol0',
		json: true
	})
    .then((data) => {
		console.log(data)
		var data1 = {
		"name":data.underName.name,
		"mobile" : data.underName.mobile
		}
		var data2 = {
		"reservationid":data.reservationId,
		"name":data.pickupLocation.name,
		"street": data.pickupLocation.address.streetAddress,
		"locality" : data.pickupLocation.address.addressLocality,
		"region" : data.pickupLocation.address.addressRegion,
		"postal" : data.pickupLocation.address.postalCode,
		"country" : data.pickupLocation.address.addressCountry,
		}
		
		mongo.connect(database.url , function(error, db){
			if (error){
				throw error;
			}
			console.log("connected to database successfully");
			console.log(data1);
			//CREATING A COLLECTION IN MONGODB USING NODE.JS
			db.collection("user").insertOne(data1, (err , collection) => {
				if(err) throw err;
				console.log("Record inserted successfully");
			});

			db.collection("address").insertOne(data2, (err , collection) => {
				if(err) throw err;
				console.log("Record inserted successfully");
			});
		});
		res.render('updated', {response: {message:'Data consumed and inserted successfully', flag:0}});
    })
    .catch((err) => {
      console.log(errors)
      res.render('error', errors)
    })
})

app.get('/download/:id', function(req, res){

  const file = "result-"+req.params.id+".csv";
  filePath = "D:/nodejs/assignment/download/";
  fs.exists(filePath, function(exists){
      if (exists) {     
        res.writeHead(200, {
          "Content-Type": "application/octet-stream",
          "Content-Disposition" : "attachment; filename=" + file});
        fs.createReadStream(filePath + file).pipe(res);
      } else {
        res.writeHead(400, {"Content-Type": "text/plain"});
        res.end("Error: File does not exists");
      }
    });  
});
app.listen(3333)